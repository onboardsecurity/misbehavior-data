% run('ML_Metrics')
% c=cvpartition(finalData(:,1),'KFold',5);
% train = finalData(c.training(1),:);
% test = finalData(c.test(1),:);
% k = (1:100);
% for i = 1:length(k)
%     KNNModel = fitcknn(train(:,1:5),train(:,6),'NumNeighbors',i);
%     labels =predict(KNNModel,test(:,1:5));
%     confusion_matrix(:,:,i) = confusionmat(labels,test(:,6));
%     ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
%     i
% end
% figure(1)
% plot(ccr)
% title('Attack Type 2 KNN Performance')
% xlabel('Number of Nearest Neighbors')
% ylabel('CCR')
% [best_ccr index] = max(ccr)
% best_confusion_matrix = confusion_matrix(:,:,index)
% 
%% METRICS
metrics = 6;
best_ccr = zeros(8,1);

best_confusion_matrix = zeros(2,2,6);

%% ATTACK 1 KNN
run('ML_Metrics_Attack1')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    all_precision(i,1) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(2,1,i));
    all_recall(i,1) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(1,2,i));
    
end
figure(1)
plot(ccr)
title('Attack Type 1 KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(1) index] = max(ccr);
best_confusion_matrix(:,:,1) = confusion_matrix(:,:,index);
K_Value(1) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;

%% ATTACK 2 KNN
run('ML_Metrics_Attack2')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    all_precision(i,2) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(2,1,i));
    all_recall(i,2) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(1,2,i));
end
figure(2)
plot(ccr)
title('Attack Type 2 KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(2) index] = max(ccr);
best_confusion_matrix(:,:,2) = confusion_matrix(:,:,index);
K_Value(2) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;
%% ATTACK 4 KNN
run('ML_Metrics_Attack1')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    all_precision(i,3) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(2,1,i));
    all_recall(i,3) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(1,2,i));
end
figure(3)
plot(ccr)
title('Attack Type 4 KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(3) index] = max(ccr);
best_confusion_matrix(:,:,3) = confusion_matrix(:,:,index);
K_Value(3) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;
%% ATTACK 8 KNN
run('ML_Metrics_Attack8')
c=cvpartition(finalData(:,1),'KFold',4);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    all_precision(i,4) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(2,1,i));
    all_recall(i,4) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(1,2,i));
end
figure(4)
plot(ccr)
title('Attack Type 8 KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(4) index] = max(ccr);
best_confusion_matrix(:,:,4) = confusion_matrix(:,:,index);
K_Value(4) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;
%% ATTACK 16 KNN
run('ML_Metrics_Attack16')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    all_precision(i,5) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(2,1,i));
    all_recall(i,5) = confusion_matrix(2,2,i)/(confusion_matrix(2,2,i)+confusion_matrix(1,2,i));
end
figure(5)
plot(ccr)
title('Attack Type 16 KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(5) index] = max(ccr);
best_confusion_matrix(:,:,5) = confusion_matrix(:,:,index);
K_Value(5) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;

%% ALL ATTACK VS. Normal  KNN
run('ML_Metrics_Attacks_vs_Normal')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    recall_knn(i) = (confusion_matrix(2,2,i))/sum(confusion_matrix(:,2,i));
    precision_knn(i) = (confusion_matrix(2,2,i))/sum(confusion_matrix(2,:,i));
end
figure(6)
plot(ccr)
title('Attack vs Normal KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(6) index] = max(ccr);
best_confusion_matrix(:,:,6) = confusion_matrix(:,:,index);

K_Value(6) = index;
clearvars -except best_ccr best_confusion_matrix K_Value metrics all_precision all_recall;


%% ALL ATTACKS
run('ML_Metrics')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
end
figure(7)
plot(ccr)
title('All The Attacks KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(7) index] = max(ccr);
best_confusion_matrix_all = confusion_matrix(:,:,index);
K_Value(7) = index;

clearvars -except best_ccr best_confusion_matrix K_Value best_confusion_matrix_all metrics all_precision all_recall;

%% ATTACK VS ATTACK

run('ML_Metrics_ClassifyAttack')
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
k = (1:100);
for i = 1:length(k)
    KNNModel = fitcknn(train(:,1:metrics),train(:,10),'NumNeighbors',i);
    labels =predict(KNNModel,test(:,1:metrics));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i)= sum(diag(confusion_matrix(:,:,i)))/length(labels);
    
end
figure(8)
plot(ccr)
title('Attacks vs Attack KNN Performance')
xlabel('Number of Nearest Neighbors')
ylabel('CCR')
[best_ccr(8) index] = max(ccr);
Attacks_Confusion_Matrix = confusion_matrix(:,:,index);
K_Value(8) = index;

clearvars -except best_ccr best_confusion_matrix K_Value best_confusion_matrix_all metrics Attacks_Confusion_Matrix all_precision all_recall;

%% TABLE
precision = zeros(8,1);
recall = zeros(8,1);
for i=1:6
   precision(i)= best_confusion_matrix(2,2,i)/(sum(best_confusion_matrix(2,:,i)));
   recall(i)= best_confusion_matrix(2,2,i)/(sum(best_confusion_matrix(:,2,i)));
end

Attack = {'Type 1','Type 2', 'Type 4', 'Type 8', 'Type 16','All vs Normal','All','Attack vs Attack'};
T=table(Attack',best_ccr,precision,recall,K_Value')


figure(9)
scatter(all_precision(:,1),all_recall(:,1))
hold on
scatter(all_precision(:,2),all_recall(:,2))
scatter(all_precision(:,3),all_recall(:,3))
scatter(all_precision(:,4),all_recall(:,4))
scatter(all_precision(:,5),all_recall(:,5))
title('ROC Curve Comparison for Different Attack Type')
ylabel('recall');
xlabel('precision')
legend('Type 1', 'Type 2','Type 4','Type 8','Type 16')
hold off


for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i)
    recall_SVM(i) = recall(1,1,i)
end