% Loads the data must change all instances of Attack1_1 in the next two
% lines
load('AttackScenario1_1.mat')
d = AttackScenario1_1;
temp = ~(isnan(d(:,1)));
d=d(temp,:);
% Initializes variables
num_metrics = 3;

%Finds the unique ID's for the car

unireceiver = unique(d(:,2)) ;

%Creates the metric matrix for all the unique cars without referencing the
%car ID
%finalData = zeros(length(unisender)*length(unireceiver),num_metrics);
x_acceleration_ci95 = [-4.6983 5.2265];
x_acceleration_ci99 = [-7.1795 7.7077];

y_acceleration_ci95 = [-8.1203 8.0501];
y_acceleration_ci99 = [-12.1629 12.0927];

z_acceleration_ci = [0 0];

%For each unique ID 
% 1.) Find all the data received from that ID
% 2.) Create the metrics based on the Car ID's entire time series data
% 3.) Record that data into the finalData

count=1;
for i=1:length(unireceiver)
    findreceiverdata=(d(:,2)== unireceiver(i));
    receiverdata = d(findreceiverdata,:);
    unisender=unique(receiverdata(:,5));
    
    for j=1:length(unisender)
        %Finds the data from the sender ID
        findsenderdata = (receiverdata(:,5)==unisender(j));
        senderdata = receiverdata(findsenderdata,:);
       
        %Creates the metrics
% %         Xaverage_acceleration =0;
% %         Yaverage_acceleration =0;
% %         Zaverage_acceleration =0;
% %         for k=1:length(senderdata(:,1))-1
% %             time = senderdata(k+1,3)-senderdata(k,3);
% %             x_acceleration = senderdata(k+1,16)-senderdata(k,16)/time;
% %             y_acceleration = senderdata(k+1,17)-senderdata(k,17)/time;
% %             z_acceleration = senderdata(k+1,18)-senderdata(k,18)/time;
% %             
% %             Xaverage_acceleration = Xaverage_acceleration + x_acceleration;
% %             Yaverage_acceleration = Yaverage_acceleration + y_acceleration;
% %             Zaverage_acceleration = Zaverage_acceleration + z_acceleration;
% %         end
% %         Xaverage_acceleration = Xaverage_acceleration / (length(senderdata(:,1))-1);
% %         Yaverage_acceleration = Yaverage_acceleration / (length(senderdata(:,1))-1);
% %         Zaverage_acceleration = Zaverage_acceleration / (length(senderdata(:,1))-1);
        max_score = 0;
        for k=1:length(senderdata(:,1))-1
            time = (senderdata(k+1,3)-senderdata(k,3));
            
            x_predicted_low95 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci95(1)));
            x_predicted_high95 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci95(2)));
            
            x_predicted_low99 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci99(1)));
            x_predicted_high99 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci99(2)));
            
            tempx = 0;
            if (x_predicted_low95 >= senderdata(k+1,7) || x_predicted_high95 <= senderdata(k+1,7))
                tempx=1; 
                if (x_predicted_low99 >= senderdata(k+1,7) || x_predicted_high99 <= senderdata(k+1,7))
                    tempx=2;
                end
            end
            
            y_predicted_low95 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci95(1)));
            y_predicted_high95 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci95(2)));
            
            y_predicted_low99 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci99(1)));
            y_predicted_high99 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci99(2)));
            
            tempy = 0;
            if (y_predicted_low95 >= senderdata(k+1,8) || y_predicted_high95 <= senderdata(k+1,8))
                tempy=1; 
                if (y_predicted_low99 >= senderdata(k+1,8) || y_predicted_high99 <= senderdata(k+1,8))
                    tempy=2;
                end
            end
            
            
            if (max_score < tempx + tempy )
               max_score = tempx+tempy; 
            end
        end
        
        
        
        
       % metric2 =
       % label = senderdata(1,22);

        %Populates the final data
       finalData(count,1)=unireceiver(i);
       finalData(count,2)=unisender(j);
%        finalData(count,3)= Xaverage_acceleration;
%        finalData(count,4)= Yaverage_acceleration;
%        finalData(count,5)= Zaverage_acceleration;
       finalData(count,3)=max_score;
       finalData(count,4)=stationarycheck;
       finalData(count,6)=senderdata(1,23);
       count = count+1;
    end
end