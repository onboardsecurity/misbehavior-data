% Loads the data must change all instances of Attack1_1 in the next two
% lines
load('AttackScenario16_1.mat')
d = Attack16_1;
temp = ~(isnan(d(:,1)));
d=d(temp,:);

% Initializes variables
num_metrics = 3;

%Finds the unique ID's for the car

unireceiver = unique(d(:,2)) ;

% Confidence interval conducted from study for acceleration at:
% 95% and 99% Confidence
x_acceleration_ci95 = [-4.6983 5.2265];
x_acceleration_ci99 = [-7.1795 7.7077];

y_acceleration_ci95 = [-8.1203 8.0501];
y_acceleration_ci99 = [-12.1629 12.0927];

z_acceleration_ci = [0 0];

%For each unique ID 
% 1.) Find all the data received from that ID
% 2.) Create the metrics based on the Car ID's entire time series data
% 3.) Record that data into the finalData

count=1;
for i=1:length(unireceiver)
    findreceiverdata=(d(:,2)== unireceiver(i));
    receiverdata = d(findreceiverdata,:);
    unisender=unique(receiverdata(:,5));
    
    for j=1:length(unisender)
        %Finds the data from the sender ID
        findsenderdata = (receiverdata(:,5)==unisender(j));
        senderdata = receiverdata(findsenderdata,:);
       
        %Creates the metrics

        %%%% METRIC 1 %%%%%%
        max_score = 0;
        for k=1:length(senderdata(:,1))-1
            time = (senderdata(k+1,3)-senderdata(k,3));
            
            x_predicted_low95 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci95(1)));
            x_predicted_high95 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci95(2)));
            
            x_predicted_low99 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci99(1)));
            x_predicted_high99 = senderdata(k,7)+ (time)*(senderdata(k,16)+(x_acceleration_ci99(2)));
            
            tempx = 0;
            if (x_predicted_low95 >= senderdata(k+1,7) || x_predicted_high95 <= senderdata(k+1,7))
                tempx=1; 
                if (x_predicted_low99 >= senderdata(k+1,7) || x_predicted_high99 <= senderdata(k+1,7))
                    tempx=2;
                end
            end
            
            y_predicted_low95 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci95(1)));
            y_predicted_high95 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci95(2)));
            
            y_predicted_low99 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci99(1)));
            y_predicted_high99 = senderdata(k,8)+ (time)*(senderdata(k,17)+(y_acceleration_ci99(2)));
            
            tempy = 0;
            if (y_predicted_low95 >= senderdata(k+1,8) || y_predicted_high95 <= senderdata(k+1,8))
                tempy=1; 
                if (y_predicted_low99 >= senderdata(k+1,8) || y_predicted_high99 <= senderdata(k+1,8))
                    tempy=2;
                end
            end
            
            
            if (max_score < tempx + tempy )
               max_score = tempx+tempy; 
            end
        end
        
        
        %%%% METRIC 2: Stationary Attacker with Changing Velocity %%%%%%
        stationarycheck = 0;
        total_x_displacement = senderdata(length(senderdata(:,1)),7) -senderdata(1,7);
        total_y_displacement = senderdata(length(senderdata(:,1)),8) -senderdata(1,8);
        total_time = senderdata(length(senderdata(:,1)),3)-senderdata(1,3);
        predicted_x_velocity = total_x_displacement/total_time; % not sure I should use
        predicted_y_velocity = total_y_displacement/total_time; % not sure I should use
        
        total_x_distance_velocity = 0;
        total_y_distance_velocity = 0;
        
        for k=2:length(senderdata(:,1))
            total_x_distance_velocity = total_x_distance_velocity + senderdata(k,16)*(senderdata(k,3)-senderdata(k-1,3));
            total_y_distance_velocity = total_y_distance_velocity + senderdata(k,17)*(senderdata(k,3)-senderdata(k-1,3));
        end
        average_x_velocity = total_x_distance_velocity/total_time;
        average_y_velocity = total_y_distance_velocity/total_time;
        
        if((total_x_displacement==0) && (average_x_velocity~=0))
            stationarycheck = 1;
        end
        if((total_y_displacement==0) && (average_y_velocity~=0))
            stationarycheck = 1;
        end
        
        
        %%%%% METRIC 3: %%%%%
  
        constant_offset_check = sqrt((average_x_velocity-predicted_x_velocity)^2+(average_y_velocity-predicted_y_velocity)^2);
        
        %%%% RAW METRICS: %%%%%
        
        total_displacement = sqrt(total_x_displacement^2+total_y_displacement^2);
        total_displacement = total_displacement - sqrt((average_x_velocity*total_time)^2+(average_y_velocity*total_time)^2);
        
        
        %Populates the final data

       finalData(count,1)=max_score;
       finalData(count,2)=stationarycheck;
       finalData(count,3)=average_x_velocity-predicted_x_velocity;
       finalData(count,4)=average_y_velocity-predicted_y_velocity;
       finalData(count,5)=constant_offset_check;
       finalData(count,6)= total_displacement;
       finalData(count,8)=unireceiver(i);
       finalData(count,9)= unisender(j);
       finalData(count,10)=senderdata(1,23);
       count = count+1;
       
    end
end

%clearvars -except finalData;
