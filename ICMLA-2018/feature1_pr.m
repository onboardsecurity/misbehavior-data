%%%%% Attack 1
run('ML_Metrics_Attack1')
clearvars -except precision recall finalData
for i=1:4
   a = (finalData(:,1) >= i)+1;
   a = a-1;
   cm = confusionmat(finalData(:,10),a);
   precision(i,1) = cm(2,2)/sum(cm(:,2));
   recall(i,1)=cm(2,2)/sum(cm(2,:));
end
clearvars -except precision recall 

%%%% Attack 2
run('ML_Metrics_Attack2')
clearvars -except precision recall finalData
for i=1:4
   a = (finalData(:,1) >= i)+1;
   a = a-1;
   a=a*2;
   cm = confusionmat(finalData(:,10),a);
   precision(i,2) = cm(2,2)/sum(cm(:,2));
   recall(i,2)=cm(2,2)/sum(cm(2,:));
end
clearvars -except precision recall

%%%% Attack 4
run('ML_Metrics_Attack4')
clearvars -except precision recall finalData

for i=1:4
   a = (finalData(:,1) >= i)+1;
   a = a-1;
   a=a*4;
   cm = confusionmat(finalData(:,10),a);
   precision(i,3) = cm(2,2)/sum(cm(:,2));
   recall(i,3)=cm(2,2)/sum(cm(2,:));
end
clearvars -except precision recall

%%%% Attack 8
run('ML_Metrics_Attack8')
clearvars -except precision recall finalData
for i=1:4
   a = (finalData(:,1) >= i)+1;
   a = a-1;
   a=a*8;
   cm = confusionmat(finalData(:,10),a);
   precision(i,4) = cm(2,2)/sum(cm(:,2));
   recall(i,4)=cm(2,2)/sum(cm(2,:));
end
clearvars -except precision recall

%%%%% Attack 16
run('ML_Metrics_Attack16')
clearvars -except precision recall finalData
for i=1:4
   a = (finalData(:,1) >= i)+1;
   a = a-1;
   a=a*16;
   cm = confusionmat(finalData(:,10),a);
   precision(i,5) = cm(2,2)/sum(cm(:,2));
   recall(i,5)=cm(2,2)/sum(cm(2,:));
end
clearvars -except precision recall