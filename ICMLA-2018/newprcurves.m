%%%%%%% KNN %%%%%%%
load('pr_knn')
figure(1)
subplot(2,1,1)
plot(all_recall(1:5:96,1),all_precision(1:5:96,1),'g')
hold on
plot(all_recall(1:5:96,2),all_precision(1:5:96,2),'b')
plot(all_recall(1:5:96,3),all_precision(1:5:96,3),'r')
plot(all_recall(1:5:96,4),all_precision(1:5:96,4),'m')
plot(all_recall(1:5:96,5),all_precision(1:5:96,5),'k')
scatter(all_recall(1:5:96,1),all_precision(1:5:96,1),10,'filled','g')
scatter(all_recall(1:5:96,2),all_precision(1:5:96,2),10,'filled','b')
scatter(all_recall(1:5:96,3),all_precision(1:5:96,3),10,'filled','r')
scatter(all_recall(1:5:96,4),all_precision(1:5:96,4),10,'filled','m')
scatter(all_recall(1:5:96,5),all_precision(1:5:96,5),10, 'filled','k')

title('Precision-Recall Curves for KNN')
xlabel('recall');
ylabel('precision')
legend('Type 1', 'Type 2','Type 4', 'Type 8', 'Type 16')
set(gca,'fontsize',14,'FontWeight','Bold')
hold off

%%%%%%% SVM %%%%%

load('SVM_Attack1_Updated')
for i=1:length(precision(1,1,:))
    precision_SVM(i,1) = precision(1,1,i);
    recall_SVM(i,1) = recall(1,1,i);
end
clearvars -except precision_SVM recall_SVM
load('SVM_Attack2_Updated')
for i=1:length(precision(1,1,:))
    precision_SVM(i,2) = precision(1,1,i);
    recall_SVM(i,2) = recall(1,1,i);
end
clearvars -except precision_SVM recall_SVM

load('SVM_Attack4_Updated')
for i=1:length(precision(1,1,:))
    precision_SVM(i,3) = precision(1,1,i);
    recall_SVM(i,3) = recall(1,1,i);
end
clearvars -except precision_SVM recall_SVM

load('SVM_Attack8_Updated')
for i=1:length(precision(1,1,:))
    precision_SVM(i,4) = precision(1,1,i);
    recall_SVM(i,4) = recall(1,1,i);
end
clearvars -except precision_SVM recall_SVM

load('SVM_Attack16_Updated')
for i=1:length(precision(1,1,:))
    precision_SVM(i,5) = precision(1,1,i);
    recall_SVM(i,5) = recall(1,1,i);
end
clearvars -except precision_SVM recall_SVM

subplot(2,1,2)
plot(recall_SVM(:,1),precision_SVM(:,1),'g')
hold on
plot(recall_SVM(:,2),precision_SVM(:,2),'b')
plot(recall_SVM(:,3),precision_SVM(:,3),'r')
plot(recall_SVM(:,4),precision_SVM(:,4),'m')
plot(recall_SVM(:,5),precision_SVM(:,5),'k')

scatter(recall_SVM(:,1),precision_SVM(:,1),10,'filled','g')
scatter(recall_SVM(:,2),precision_SVM(:,2),10,'filled','b')
scatter(recall_SVM(:,3),precision_SVM(:,3),10,'filled','r')
scatter(recall_SVM(:,4),precision_SVM(:,4),10,'filled','m')
scatter(recall_SVM(:,5),precision_SVM(:,5),10,'filled','k')
title('Precision-Recall Curves for SVM')
xlabel('recall');
ylabel('precision')
legend('Type 1', 'Type 2','Type 4', 'Type 8', 'Type 16')
set(gca,'fontsize',14,'FontWeight','Bold')
