run('ML_Metrics_Attack16')
clearvars -except finalData
c=cvpartition(finalData(:,1),'KFold',5);
train = finalData(c.training(1),:);
test = finalData(c.test(1),:);
c=(-9:10);
%i=13;
for i=1:length(c)
    SVMModel=fitcsvm(train(:,1:6),train(:,10),'KernelFunction','rbf','BoxConstraint',2^(c(i)));
    labels=predict(SVMModel,test(:,1:6));
    confusion_matrix(:,:,i) = confusionmat(labels,test(:,10));
    ccr(i) = sum(diag(confusion_matrix(:,:,i)))/length(labels);
    i
end
