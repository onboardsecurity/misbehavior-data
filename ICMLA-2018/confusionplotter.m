A = [0.7505 0.0523 0.0015 0.0544 0.4408
    0.0189 0.7714 0 0.0223 0.2032
    0.1201 0.1187 0.9917 0.1441 0.1173
    0.0162 0.0093 0.0068 0.7722 0.0189
    0.0942 0.0484 0 0.0071 0.2198];
imshow(A, [], 'InitialMagnification', 1600);
colorbar;

xlabel('Ground Truth')
ylabel('Predicted')

axis on;
xticks([1 2 3 4 5])
yticks([1 2 3 4 5])

xticklabels({'Type 1','Type 2','Type 4','Type 8','Type 16'})
yticklabels({'Type 1','Type 2','Type 4','Type 8','Type 16'})

set(gca,'fontsize',14,'FontWeight','Bold')
colormap(gca,'summer')
% Set up figure properties:
% Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
% Get rid of tool bar and pulldown menus that are along top of figure.
set(gcf, 'Toolbar', 'none', 'Menu', 'none');
% Give a name to the title bar.
title('Confusion Matrix of Attack Types')
set(gcf, 'Name', 'Confusion Matrix of Attack Types', 'NumberTitle', 'Off')