load('pr_knn')
load('SVM_Attack1_Updated')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(1)
%subplot(3,2,1)
plot(all_recall(1:5:96,1),all_precision(1:5:96,1))
hold on
plot(recall_SVM,precision_SVM)

%plot(recall_f1(:,1),precision_f1(:,1));
%scatter(recall_f1(:,1),precision_f1(:,1));

scatter(all_recall(1:5:96,1),all_precision(1:5:96,1))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Type 1')
xlabel('recall');
ylabel('precision')
legend('KNN', 'SVM')
%legend('KNN', 'SVM','Feature 1')
set(gca,'fontsize',14,'FontWeight','Bold')

%xlim([0 1])
%ylim([0 1])

hold off
clearvars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('pr_knn')
load('SVM_Attack2_Updated')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(2)

%figure(1)
%subplot(3,2,2)

plot(all_recall(1:5:96,2),all_precision(1:5:96,2))
hold on
plot(recall_SVM,precision_SVM)

%plot(recall_f1(:,2),precision_f1(:,2));
%scatter(recall_f1(:,2),precision_f1(:,2));

scatter(all_recall(1:5:96,2),all_precision(1:5:96,2))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Type 2')
xlabel('recall');
ylabel('precision')

legend('KNN', 'SVM')
%legend('KNN', 'SVM','Feature 1')
set(gca,'fontsize',14,'FontWeight','Bold')
%xlim([0 1])
%ylim([0 1])

hold off
clearvars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('pr_knn')
load('SVM_Attack4_Updated')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(3)
%figure(1)
%subplot(3,2,3)

plot(all_recall(1:5:96,3),all_precision(1:5:96,3))
hold on
plot(recall_SVM,precision_SVM)
%plot(recall_f1(:,3),precision_f1(:,3));
%scatter(recall_f1(:,3),precision_f1(:,3));

scatter(all_recall(1:5:96,3),all_precision(1:5:96,3))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Type 4')
xlabel('recall');
ylabel('precision')
legend('KNN', 'SVM')
%legend('KNN', 'SVM','Feature 1')
set(gca,'fontsize',14,'FontWeight','Bold')
%xlim([0 1])
%ylim([0 1])

hold off
clearvars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('pr_knn')
load('SVM_Attack8_Updated')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(4)
%figure(1)
%subplot(3,2,4)

plot(all_recall(1:5:96,4),all_precision(1:5:96,4))
hold on
plot(recall_SVM,precision_SVM)
%plot(recall_f1(:,4),precision_f1(:,4));
%scatter(recall_f1(:,4),precision_f1(:,4));

scatter(all_recall(1:5:96,4),all_precision(1:5:96,4))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Type 8')
xlabel('recall');
ylabel('precision')
legend('KNN', 'SVM')
%legend('KNN', 'SVM','Feature 1')
set(gca,'fontsize',14,'FontWeight','Bold')
%xlim([0 1])
%ylim([0 1])

hold off
clearvars
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('pr_knn')
load('SVM_Attack16_Updated')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(5)
%figure(1)
%subplot(3,2,5)
plot(all_recall(1:5:96,5),all_precision(1:5:96,5))
hold on
plot(recall_SVM,precision_SVM)
%plot(recall_f1(:,5),precision_f1(:,5));
%scatter(recall_f1(:,5),precision_f1(:,5));
scatter(all_recall(1:5:96,5),all_precision(1:5:96,5))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Type 16')
xlabel('recall');
ylabel('precision')
set(gca,'fontsize',14,'FontWeight','Bold')
legend('KNN', 'SVM')
%legend('KNN', 'SVM','Feature 1')

%xlim([0 1])
%ylim([0 1])

hold off
clearvars

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load('pr_knn_attackdetection')
load('ML_Metrics_Normal_vs_Attacker')
load('f1_pr')
for i=1:length(precision(1,1,:))
    precision_SVM(i) = precision(1,1,i);
    recall_SVM(i) = recall(1,1,i);
end


figure(6)
%figure(1)
%subplot(3,2,5)
plot(recall_knn(1:5:96),precision_knn(1:5:96))
hold on
plot(recall_SVM,precision_SVM)
scatter(recall_knn(1:5:96),precision_knn(1:5:96))
scatter(recall_SVM,precision_SVM)
title('Precision-Recall Comparison for Attack Detection')
xlabel('recall');
ylabel('precision')
set(gca,'fontsize',14,'FontWeight','Bold')
legend('KNN', 'SVM')

%xlim([0 1])
%ylim([0 1])

hold off
clearvars

